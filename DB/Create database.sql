Create database TestDatabase;

use TestDataBase;

Create table Customer(
CustomerID int not null Identity primary key,
Name nvarchar(50) null,
Address1 nvarchar(255) null,
Address2 nvarchar(255) null,
ZipCode varchar(10) null,
City nvarchar(50) null,
CountryID int null,
Email varchar(255) null,
Phone nvarchar(50) null,
Mobile nvarchar(50) null,
BirthYear int null,
Gender smallint null,
SiteID nvarchar(50) null,
LocalCustomerID int null,
PermissionStatus bit null,
Country nvarchar(50) null,
CreatedBy int null,
CreationDate datetime null,
UpdatedBy int null,
UpdateDate datetime null,
CreatorTypeID int null,
UpdaterTypeID int null,
IsMerged bit null,
MergedCustomerID int null,
Category varchar(50) null,
Interests varchar(2048) null
)

Create table Card(
CardID int not null Identity primary key,
CardNumber bigint not null,
CardNumberBase varchar(26) null,
CustomerID int not null foreign key references Customer(CustomerID),
BirthYear int null,
IssueDate datetime not null,
ExpirationDate datetime not null,
SiteID varchar(50) null,
LocalCustomerID int null,
CardType varchar(20) null,
Name nvarchar(50) null,
Address1 nvarchar(255) null,
ZipCode varchar(10) null,
City nvarchar(50) null,
Country int null,
Email varchar(255) null,
Phone nvarchar(50) null,
Mobile nvarchar(50) null,
Gender smallint null,
PermissionStatus bit null,
Active bit null
)
Create table Booking(
BookingID int not null Identity primary key,
CustomerID int not null foreign key references Customer(CustomerID),
CardID int null foreign key references Card(CardID),
BookingDate datetime not null,
FirstNightDate datetime not null,
NumberOfDate datetime not null,
NumberOfDays int not null,
LocalCastomerID int not null,
SiteID varchar(50) not null,
CBBookingID int not null,
Status varchar(20) null,
Price money null,
TypeOfRoom varchar(50) null,
SendToCsv bit null,
Capaign varchar(50) null,
Category varchar(50) null
)