use TestDatabase;
go
CREATE PROCEDURE pr_CardInsert
	@CardNumber bigint,
	@CardNumberBase varchar(26),
	@CustomerID int,
	@BirthYear int,
	@IssueDate datetime,
	@ExpirationDate datetime,
	@SiteID varchar(50),
	@LocalCustomerID int,
	@CardType varchar(20),
	@Name nvarchar(50),
	@Address1 nvarchar(255),
	@ZipCode varchar(10) ,
	@City nvarchar(50),
	@Country int,
	@Email varchar(255),
	@Phone nvarchar(50),
	@Mobile nvarchar(50),
	@Gender smallint,
	@PermissionStatus bit,
	@Active bit
as
INSERT INTO 
dbo.Card
(CardNumber,CardNumberBase,CustomerID,BirthYear,IssueDate,ExpirationDate ,
	SiteID,LocalCustomerID ,CardType ,Name,Address1,ZipCode,City,Country,
	Email,Phone,Mobile,Gender,PermissionStatus,Active) 
VALUES (	@CardNumber,@CardNumberBase,@CustomerID,@BirthYear,@IssueDate,@ExpirationDate ,
	@SiteID,@LocalCustomerID ,@CardType ,@Name,@Address1,@ZipCode,@City,@Country,
	@Email,@Phone,@Mobile,@Gender,@PermissionStatus,@Active) 
go

go
CREATE PROCEDURE pr_CardUpdate
	@CardNumber bigint,
	@CardNumberBase varchar(26),
	@CustomerID int,
	@BirthYear int,
	@IssueDate datetime,
	@ExpirationDate datetime,
	@SiteID varchar(50),
	@LocalCustomerID int,
	@CardType varchar(20),
	@Name nvarchar(50),
	@Address1 nvarchar(255),
	@ZipCode varchar(10) ,
	@City nvarchar(50),
	@Country int,
	@Email varchar(255),
	@Phone nvarchar(50),
	@Mobile nvarchar(50),
	@Gender smallint,
	@PermissionStatus bit,
	@Active bit,
	@CardID int
as
UPDATE dbo.Card SET
	CardNumber = @CardNumber,
	CardNumberBase = @CardNumberBase,
	CustomerID = @CustomerID,
	BirthYear = @BirthYear,
	IssueDate = @IssueDate,
	ExpirationDate =@ExpirationDate,
	SiteID = @SiteID,
	LocalCustomerID  = @LocalCustomerID,
	CardType=@CardType,
	Name = @Name,
	Address1 = @Address1,
	ZipCode = @ZipCode,
	City = @City,
	Country = @Country,
	Email = @Email,
	Phone = @Phone,
	Mobile = @Mobile,
	Gender = @Gender,
	PermissionStatus = @PermissionStatus,
	Active = @Active
where CardID = @CardID;
go



go
create procedure pr_CardLoadAll as
begin transaction
select * from dbo.Card 
COMMIT TRANSACTION
go

go
create procedure pr_CardLoadByNumber
				@Number bigint as
begin transaction
select * from dbo.Card where dbo.Card.CardNumber = @Number
COMMIT TRANSACTION
go

go
create procedure pr_CardLoadByID
				@ID int as
begin transaction
select * from dbo.Card where dbo.Card.CardID = @ID
COMMIT TRANSACTION
go

go
create procedure pr_CardLoadByCustomerID
				@CustomerID int as
begin transaction
select * from dbo.Card where dbo.Card.CustomerID = @CustomerID
COMMIT TRANSACTION
go

go
create procedure pr_CardLoadBySiteId
				@SiteID nvarchar(255) as
begin transaction
select * from dbo.Card where dbo.Card.SiteID = @SiteID
COMMIT TRANSACTION
go