
use TestDataBase;



go
CREATE PROCEDURE pr_CustomerInsert

	@Name nvarchar(50),
	@Address1 nvarchar(255),
	@Address2 nvarchar(255),
	@ZipCode varchar(10),
	@City nvarchar(50),
	@CountryID int,
	@Email varchar(255),
	@Phone nvarchar(50),
	@Mobile nvarchar(50),
	@BirthYear int,
	@Gender smallint,
	@SiteID nvarchar(50),
	@LocalCustomerID int,
	@PermissionStatus bit,
	@Country nvarchar(50),
	@CreatedBy int,
	@CreationDate datetime,
	@UpdatedBy int,
	@UpdateDate datetime,
	@CreatorTypeID int,
	@UpdaterTypeID int,
	@IsMerged bit,
	@MergedCustomerID int,
	@Category varchar(50),
	@Interests varchar(2048)
as
INSERT INTO 
dbo.Customer 
(Name, Address1, Address2, ZipCode, City, CountryID, Email, Phone, Mobile, BirthYear,
Gender, SiteID, LocalCustomerID, PermissionStatus, Country, CreatedBy, CreationDate,
UpdatedBy, UpdateDate, CreatorTypeID, UpdaterTypeID, IsMerged, MergedCustomerID, Category, Interests) 
VALUES (@Name, @Address1, @Address2, @ZipCode, @City, @CountryID, @Email, @Phone, @Mobile, @BirthYear,
@Gender, @SiteID, @LocalCustomerID, @PermissionStatus, @Country, @CreatedBy, @CreationDate,
@UpdatedBy, @UpdateDate, @CreatorTypeID, @UpdaterTypeID, @IsMerged, @MergedCustomerID, @Category, @Interests) ;
go

go
CREATE PROCEDURE pr_CustomerUpdate
	@Name nvarchar(50),
	@Address1 nvarchar(255),
	@Address2 nvarchar(255),
	@ZipCode varchar(10),
	@City nvarchar(50),
	@CountryID int,
	@Email varchar(255),
	@Phone nvarchar(50),
	@Mobile nvarchar(50),
	@BirthYear int,
	@Gender smallint,
	@SiteID nvarchar(50),
	@LocalCustomerID int,
	@PermissionStatus bit,
	@Country nvarchar(50),
	@CreatedBy int,
	@CreationDate datetime,
	@UpdatedBy int,
	@UpdateDate datetime,
	@CreatorTypeID int,
	@UpdaterTypeID int,
	@IsMerged bit,
	@MergedCustomerID int,
	@Category varchar(50),
	@Interests varchar(2048),
	@CustomerID int
as
UPDATE dbo.Customer SET
	Name = @Name, 
	Address1 = @Address1,
	Address2 = @Address2,
	ZipCode = @ZipCode,
	City = @City, 
	CountryID = @CountryID,
	Email = @Email,
	Phone = @Phone,
	Mobile = @Mobile, 
	BirthYear = @BirthYear,
	Gender = @Gender, 
	SiteID = @SiteID, 
	LocalCustomerID = @LocalCustomerID,
	PermissionStatus = @PermissionStatus, 
	Country = @Country, 
	CreatedBy = @CreatedBy, 
	CreationDate = @CreationDate,
	UpdatedBy = @UpdatedBy, 
	UpdateDate = @UpdateDate, 
	CreatorTypeID = @CreatorTypeID, 
	UpdaterTypeID = @UpdaterTypeID, 
	IsMerged = @IsMerged, 
	MergedCustomerID = @MergedCustomerID, 
	Category = @Category, 
	Interests = @Interests 
where CustomerID = @CustomerID;
go

go
create procedure pr_CustomerLoadAll as
begin transaction
select * from dbo.Customer 
COMMIT TRANSACTION
go

go
create procedure pr_CustomerLoadAllWithCards as
begin transaction
select * from dbo.Customer 
join dbo.Card on dbo.Customer.CustomerID = dbo.Card.CustomerID
COMMIT TRANSACTION
go

go
create procedure pr_CustomerLoadByEmail
				@Email varchar(255) as
begin transaction
select * from dbo.Customer where dbo.Customer.Email = @Email
COMMIT TRANSACTION
go

go
create procedure pr_CustomerLoadByID
				@ID int as
begin transaction
select * from dbo.Customer where dbo.Customer.CustomerID = @ID
COMMIT TRANSACTION
go

go
create procedure pr_CustomerLoadByName
				@Name varchar(255) as
begin transaction
select * from dbo.Customer where dbo.Customer.Name = @Name
COMMIT TRANSACTION
go

go
create procedure pr_CustomerLoadByPhone
				@Phone nvarchar(255) as
begin transaction
select * from dbo.Customer where dbo.Customer.Phone = @Phone
COMMIT TRANSACTION
go

go
create procedure pr_CustomerLoadBySiteIDAndLocalCustomerID
				@SiteID nvarchar(255),
				@LocalCustomerID int as
begin transaction
select * from dbo.Customer where dbo.Customer.SiteID = @SiteID AND 
								dbo.Customer.LocalCustomerID = @LocalCustomerID
COMMIT TRANSACTION
go