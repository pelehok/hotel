use TestDatabase
go
CREATE PROCEDURE pr_BookingInsert
	@CustomerID int,
	@CardID int,
	@BookingDate datetime,
	@FirstNightDate datetime,
	@NumberOfDate datetime,
	@NumberOfDays int,
	@LocalCastomerID int,
	@SiteID varchar(50),
	@CBBookingID int,
	@Status varchar(20),
	@Price money,
	@TypeOfRoom varchar(50),
	@SendToCsv bit,
	@Capaign varchar(50),
	@Category varchar(50)
as
INSERT INTO 
dbo.Booking
(	CustomerID,
	CardID,
	BookingDate,
	FirstNightDate,
	NumberOfDate,
	NumberOfDays,
	LocalCastomerID,
	SiteID,
	CBBookingID,
	Status ,
	Price,
	TypeOfRoom,
	SendToCsv,
	Capaign,
	Category) 
VALUES (@CustomerID,
	@CardID,
	@BookingDate,
	@FirstNightDate,
	@NumberOfDate,
	@NumberOfDays,
	@LocalCastomerID,
	@SiteID,
	@CBBookingID,
	@Status ,
	@Price,
	@TypeOfRoom,
	@SendToCsv,
	@Capaign,
	@Category) 
go

go
CREATE PROCEDURE pr_BookingUpdate
	@CustomerID int,
	@CardID int,
	@BookingDate datetime,
	@FirstNightDate datetime,
	@NumberOfDate datetime,
	@NumberOfDays int,
	@LocalCastomerID int,
	@SiteID varchar(50),
	@CBBookingID int,
	@Status varchar(20),
	@Price money,
	@TypeOfRoom varchar(50),
	@SendToCsv bit,
	@Capaign varchar(50),
	@Category varchar(50),
	@BookingID int
as
UPDATE dbo.Booking SET
	CustomerID = @CustomerID,
	CardID = @CardID,
	BookingDate = @BookingDate,
	FirstNightDate = @FirstNightDate,
	NumberOfDate = @NumberOfDate,
	NumberOfDays = @NumberOfDays,
	LocalCastomerID = @LocalCastomerID,
	SiteID = @SiteID,
	CBBookingID = @CBBookingID,
	Status = @Status,
	Price = @Price,
	TypeOfRoom =@TypeOfRoom,
	SendToCsv = @SendToCsv,
	Capaign = @Capaign,
	Category = @Category
where BookingID = @BookingID;
go

go
create procedure pr_BookingLoadAll as
begin transaction
select * from dbo.Booking 
COMMIT TRANSACTION
go

go
create procedure pr_BookingLoadByID
				@ID int as
begin transaction
select * from dbo.Booking where dbo.Booking.BookingID = @ID
COMMIT TRANSACTION
go

go
create procedure pr_BookingLoadByCustomerID
				@CustomerID int as
begin transaction
select * from dbo.Booking where dbo.Booking.CustomerID = @CustomerID
COMMIT TRANSACTION
go