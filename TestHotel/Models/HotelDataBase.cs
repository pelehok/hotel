namespace TestHotel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class HotelDataBase : DbContext
    {
        public HotelDataBase()
            : base("name=HotelDataBase1")
        {
        }

        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Card> Cards { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Booking>()
                .Property(e => e.SiteID)
                .IsUnicode(false);

            modelBuilder.Entity<Booking>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<Booking>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Booking>()
                .Property(e => e.TypeOfRoom)
                .IsUnicode(false);

            modelBuilder.Entity<Booking>()
                .Property(e => e.Capaign)
                .IsUnicode(false);

            modelBuilder.Entity<Booking>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<Card>()
                .Property(e => e.CardNumberBase)
                .IsUnicode(false);

            modelBuilder.Entity<Card>()
                .Property(e => e.SiteID)
                .IsUnicode(false);

            modelBuilder.Entity<Card>()
                .Property(e => e.CardType)
                .IsUnicode(false);

            modelBuilder.Entity<Card>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<Card>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Interests)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Bookings)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Cards)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);
        }
    }
}
