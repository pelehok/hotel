﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestHotel.Models
{
    public class CreateOrEditModel<T>
    {
        public T value { get; set; }
        public string Message { get; set; }
    }
}