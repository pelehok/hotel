namespace TestHotel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            Bookings = new HashSet<Booking>();
            Cards = new HashSet<Card>();
        }

        public int CustomerID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [StringLength(255)]
        [Display(Name = "First address")]
        public string Address1 { get; set; }

        [StringLength(255)]
        [Display(Name = "Secont address")]
        public string Address2 { get; set; }

        [StringLength(10)]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        public int? CountryID { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        [Display(Name = "Birth year")]
        public int? BirthYear { get; set; }

        public short? Gender { get; set; }

        [StringLength(50)]
        [Display(Name = "Id of hotel")]
        public string SiteID { get; set; }

        [Display(Name = "Administrator of hotel")]
        public int? LocalCustomerID { get; set; }

        [Display(Name = "Permission Status")]
        public bool? PermissionStatus { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreationDate { get; set; }

        public int? UpdatedBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? CreatorTypeID { get; set; }

        public int? UpdaterTypeID { get; set; }

        public bool? IsMerged { get; set; }

        public int? MergedCustomerID { get; set; }

        [StringLength(50)]
        public string Category { get; set; }

        [StringLength(2048)]
        public string Interests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking> Bookings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Card> Cards { get; set; }
    }
}
