﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestHotel.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                List<Customer> customer = db.Customers.ToList();
                return View(customer);
            }
        }
    }
}