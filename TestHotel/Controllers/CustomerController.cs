﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestHotel.Models;

namespace TestHotel.Controllers
{
    public class CustomerController : Controller
    {
        #region Customer
        // GET: Customer
        public ActionResult CustomerLoadAll()
        {
            if (Request.IsAuthenticated)
            {
                using (HotelDataBase db = new HotelDataBase())
                {
                    List<Customer> customers = db.Customers.ToList();
                    return View(customers);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult CustomerDetail(int id)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Customer customer = (from p in db.Customers
                                    where p.CustomerID == id
                                    select p).First();
                return View(customer);
            }
        }

        public ActionResult CustomerCreate(int id)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Customer customer = (from p in db.Customers
                                     where p.CustomerID == id
                                     select p).FirstOrDefault();
                if (customer == null)
                {
                    ViewBag.Message = "Create";
                    return View(new Customer());
                }
                else
                {
                    ViewBag.Message = "Edit";
                    return View(customer);
                }
            }
        }

        [HttpPost]
        public ActionResult CustomerCreate(Customer customer)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Customer tempCustomer = (from p in db.Customers
                                     where p.CustomerID == customer.CustomerID
                                     select p).FirstOrDefault();
                if (tempCustomer == null)//create
                {
                    db.Customers.Add(customer);
                    db.SaveChanges();
                }
                else//edit
                {
                    db.Customers.Remove(tempCustomer);

                    //customer.Cards = db.Cards.Where(c => c.CustomerID == customer.CustomerID)
                    //    .ToList();

                    //customer.Bookings = db.Bookings.Where(c => c.CustomerID == customer.CustomerID)
                    //    .ToList();

                    db.Cards.Where(c => c.CustomerID == customer.CustomerID)
                        .ToList()
                        .ForEach(p=>p.Customer = customer);

                    db.Bookings.Where(c => c.CustomerID == customer.CustomerID)
                        .ToList()
                        .ForEach(p=>p.Customer = customer);

                    db.Customers.Add(customer);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("CustomerLoadAll");
        }

        public ActionResult CustomerDelete(int id)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Customer customer = db.Customers
                    .Where(c => c.CustomerID == id)
                    .FirstOrDefault();

                db.Bookings.Where(p => p.CustomerID == id)
                    .ToList()
                    .ForEach(p => db.Bookings.Remove(p));

                db.Cards.Where(p => p.CustomerID == id)
                    .ToList()
                    .ForEach(p => db.Cards.Remove(p));

                db.Customers.Remove(customer);
                db.SaveChanges();
            }
            return RedirectToAction("CustomerLoadAll");
        }
        
        #endregion

        #region Card
        public ActionResult CardLoadByCustomerIdOrAll(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    using (HotelDataBase db = new HotelDataBase())
                    { 
                        List<Card> cards = (from s in db.Cards
                                            .Include("Customer") select s)
                                            .ToList();
                        return View(cards);
                    }
                }
                else
                {
                    List<Card> cardByCustomerId = new List<Card>();

                    using (HotelDataBase db = new HotelDataBase())
                    {
                        cardByCustomerId = (from p in db.Cards.Include("Customer")
                                            where p.CustomerID == id
                                            select p).ToList();
                        return View(cardByCustomerId);
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            
        }

        public ActionResult CardDetail(int id)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Card card = (from p in db.Cards
                                     where p.CardID == id
                                     select p).First();
                return View(card);
            }
        }

        public ActionResult CardCreate(int id)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Card card = (from p in db.Cards.Include("Customer")
                                     where p.CustomerID == id
                                     select p).FirstOrDefault();
                if (card == null)
                {
                    ViewBag.Message = "Create";
                    return View(new Card());
                }
                else
                {
                    ViewBag.Message = "Edit";
                    return View(card);
                }
            }
        }

        [HttpPost]
        public ActionResult CardCreate(Card card)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Card tempCard = (from p in db.Cards
                                         where p.CardID == card.CardID
                                         select p).FirstOrDefault();
                if (tempCard == null)//create
                {
                    Customer customerWithIdFromCard = db.Customers
                        .Where(c => c.CustomerID == card.CustomerID)
                        .FirstOrDefault();
                    if (customerWithIdFromCard == null)
                    {
                        return RedirectToAction("Error", "We don't have customer with such CustomerId");
                    }
                    else
                    {
                        //customerWithIdFromCard.Cards.Add(card);
                        card.Customer = customerWithIdFromCard;

                        db.Cards.Add(card);
                        db.SaveChanges();
                    }
                }
                else//edit
                {
                    Customer customerWithIdFromCard = db.Customers
                        .Where(c => c.CustomerID == card.CustomerID)
                        .FirstOrDefault();

                    db.Cards.Remove(tempCard);
                    db.Cards.Add(card);

                    //customerWithIdFromCard.Cards.Add(card);
                    card.Customer = customerWithIdFromCard;

                    db.Bookings.Where(c => c.CardID == card.CustomerID)
                        .ToList()
                        .ForEach(p => p.Card = card);

                    db.SaveChanges();
                }
            }
            return RedirectToAction("CardLoadByCustomerIdOrAll");
        }

        public ActionResult CardDelete(int id)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Card card = db.Cards
                    .Where(c => c.CardID == id)
                    .FirstOrDefault();

                db.Cards.Remove(card);
                db.SaveChanges();
            }
            return RedirectToAction("CustomerLoadAll");
        }

        #endregion

        #region Booking
        public ActionResult BookingLoadByCustomerIdOrAll(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    using (HotelDataBase db = new HotelDataBase())
                    {
                        List<Booking> bookings = db.Bookings.ToList();
                        return View(bookings);
                    }
                }
                else
                {
                    List<Booking> bookingByCustomerId = new List<Booking>();

                    using (HotelDataBase db = new HotelDataBase())
                    {
                        bookingByCustomerId = (from p in db.Bookings
                                            where p.CustomerID == id
                                            select p).ToList();
                        return View(bookingByCustomerId);
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }
        public ActionResult BookingCreate(int id)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Booking booking = (from p in db.Bookings
                             where p.CustomerID == id
                             select p).FirstOrDefault();
                if (booking == null)
                {
                    ViewBag.Message = "Create";
                    return View(new Booking());
                }
                else
                {
                    ViewBag.Message = "Edit";
                    return View(booking);
                }
            }
        }
        [HttpPost]
        public ActionResult BookingCreate(Booking booking)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Booking tempbooking = (from p in db.Bookings
                                 where p.BookingID == booking.BookingID
                                 select p).FirstOrDefault();
                if (tempbooking == null)//create
                {
                    Customer customerWithIdFromBooking = db.Customers
                        .Where(c => c.CustomerID == booking.CustomerID)
                        .FirstOrDefault();
                    Card CardWithIdFromBooking = db.Cards
                        .Where(c => c.CardID == booking.CardID)
                        .FirstOrDefault();
                    if (customerWithIdFromBooking == null || CardWithIdFromBooking==null)
                    {
                        return RedirectToAction("Error", "We don't have customer with such CustomerId or card with such cardID");
                    }
                    else
                    {
                        //customerWithIdFromCard.Cards.Add(card);
                        booking.Customer = customerWithIdFromBooking;
                        booking.Card = CardWithIdFromBooking;

                        db.Bookings.Add(booking);
                        db.SaveChanges();
                    }
                }
                else//edit
                {
                    Customer customerWithIdFromBooking = db.Customers
                        .Where(c => c.CustomerID == booking.CustomerID)
                        .FirstOrDefault();

                    Card CardWithIdFromBooking = db.Cards
                        .Where(c => c.CardID == booking.CardID)
                        .FirstOrDefault();

                    db.Bookings.Remove(tempbooking);
                    db.Bookings.Add(booking);

                    //customerWithIdFromCard.Cards.Add(card);
                    booking.Customer = customerWithIdFromBooking;
                    booking.Card = CardWithIdFromBooking;

                    db.SaveChanges();
                }
            }
            return RedirectToAction("BookingLoadByCustomerIdOrAll");
        }
        public ActionResult BookingDelete(int id)
        {
            using (HotelDataBase db = new HotelDataBase())
            {
                Booking booking = db.Bookings
                    .Where(c => c.BookingID == id)
                    .FirstOrDefault();

                db.Bookings.Remove(booking);
                db.SaveChanges();
            }
            return RedirectToAction("CustomerLoadAll");
        }
        public ActionResult Error(string Message)
        {
            ViewBag.ErrorMessage = Message;
            return View();
        }
        #endregion

    }
}